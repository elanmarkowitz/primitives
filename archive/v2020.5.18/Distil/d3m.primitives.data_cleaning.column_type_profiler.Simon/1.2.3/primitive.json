{
    "id": "d2fa8df2-6517-3c26-bafc-87b701c4043a",
    "version": "1.2.3",
    "name": "simon",
    "keywords": [
        "Data Type Predictor",
        "Semantic Classification",
        "Text",
        "NLP",
        "Tabular"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@kungfu.ai",
        "uris": [
            "https://github.com/kungfuai/d3m-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package": "cython",
            "version": "0.29.16"
        },
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/kungfuai/d3m-primitives.git@2291f9115b3aba3422f55a6c743f50f98bf96a96#egg=kf-d3m-primitives"
        },
        {
            "type": "TGZ",
            "key": "simon_models_1",
            "file_uri": "http://public.datadrivendiscovery.org/simon_models_1.tar.gz",
            "file_digest": "d071106b823ab1168879651811dd03b829ab0728ba7622785bb5d3541496c45f"
        }
    ],
    "python_path": "d3m.primitives.data_cleaning.column_type_profiler.Simon",
    "algorithm_types": [
        "CONVOLUTIONAL_NEURAL_NETWORK"
    ],
    "primitive_family": "DATA_CLEANING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "kf_d3m_primitives.data_preprocessing.data_typing.simon.SimonPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "kf_d3m_primitives.data_preprocessing.data_typing.simon.Params",
            "Hyperparams": "kf_d3m_primitives.data_preprocessing.data_typing.simon.Hyperparams"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "detect_semantic_types": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [
                    "http://schema.org/Boolean",
                    "https://metadata.datadrivendiscovery.org/types/CategoricalData",
                    "http://schema.org/Integer",
                    "http://schema.org/Float",
                    "http://schema.org/Text",
                    "http://schema.org/DateTime",
                    "https://metadata.datadrivendiscovery.org/types/Time",
                    "https://metadata.datadrivendiscovery.org/types/OrdinalData",
                    "https://metadata.datadrivendiscovery.org/types/AmericanPhoneNumber",
                    "http://schema.org/addressCountry",
                    "http://schema.org/Country",
                    "http://schema.org/longitude",
                    "http://schema.org/latitude",
                    "http://schema.org/postalCode",
                    "http://schema.org/City",
                    "http://schema.org/State",
                    "http://schema.org/address",
                    "http://schema.org/email",
                    "https://metadata.datadrivendiscovery.org/types/FileName",
                    "https://metadata.datadrivendiscovery.org/types/UniqueKey",
                    "https://metadata.datadrivendiscovery.org/types/Attribute",
                    "https://metadata.datadrivendiscovery.org/types/TrueTarget",
                    "https://metadata.datadrivendiscovery.org/types/UnknownType",
                    "https://metadata.datadrivendiscovery.org/types/PrimaryKey",
                    "https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey"
                ],
                "structural_type": "typing.Sequence[str]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of semantic types to detect and set. One can provide a subset of supported semantic types to limit what the primitive detects.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Enumeration",
                    "default": "http://schema.org/Boolean",
                    "structural_type": "str",
                    "semantic_types": [],
                    "values": [
                        "http://schema.org/Boolean",
                        "https://metadata.datadrivendiscovery.org/types/CategoricalData",
                        "http://schema.org/Integer",
                        "http://schema.org/Float",
                        "http://schema.org/Text",
                        "http://schema.org/DateTime",
                        "https://metadata.datadrivendiscovery.org/types/Time",
                        "https://metadata.datadrivendiscovery.org/types/OrdinalData",
                        "https://metadata.datadrivendiscovery.org/types/AmericanPhoneNumber",
                        "http://schema.org/addressCountry",
                        "http://schema.org/Country",
                        "http://schema.org/longitude",
                        "http://schema.org/latitude",
                        "http://schema.org/postalCode",
                        "http://schema.org/City",
                        "http://schema.org/State",
                        "http://schema.org/address",
                        "http://schema.org/email",
                        "https://metadata.datadrivendiscovery.org/types/FileName",
                        "https://metadata.datadrivendiscovery.org/types/UniqueKey",
                        "https://metadata.datadrivendiscovery.org/types/Attribute",
                        "https://metadata.datadrivendiscovery.org/types/TrueTarget",
                        "https://metadata.datadrivendiscovery.org/types/UnknownType",
                        "https://metadata.datadrivendiscovery.org/types/PrimaryKey",
                        "https://metadata.datadrivendiscovery.org/types/PrimaryMultiKey"
                    ]
                },
                "is_configuration": false,
                "min_size": 0
            },
            "remove_unknown_type": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Remove \"https://metadata.datadrivendiscovery.org/types/UnknownType\" semantic type from columns on which the primitive has detected other semantic types."
            },
            "use_columns": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of column indices to force primitive to operate on. If any specified column cannot be detected, it is skipped.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            },
            "exclude_columns": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            },
            "return_result": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "replace",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Should detected columns be appended, should they replace original columns, or should only detected columns be returned?",
                "values": [
                    "append",
                    "replace",
                    "new"
                ]
            },
            "add_index_columns": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\"."
            },
            "replace_index_columns": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Replace primary index columns even if otherwise appending columns. Applicable only if \"return_result\" is set to \"append\"."
            },
            "overwrite": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to overwrite manual annotations with SIMON annotations. If overwrite is set to Falseonly columns with `UnknownType` will be processed, otherwise all columns will be processed"
            },
            "statistical_classification": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to infer categorical and ordinal annotations using rule-based classification"
            },
            "multi_label_classification": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "whether to perfrom multi-label classification and potentially append multiple annotations to metadata."
            },
            "max_rows": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 500,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "maximum number of rows from the dataset to process when inferring column semantic types",
                "lower": 100,
                "upper": 2000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "p_threshold": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.9,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "probability threshold to use when decoding classification results. Semantic types with prediction probabilities above `p_threshold`will be added",
                "lower": 0,
                "upper": 1.0,
                "lower_inclusive": true,
                "upper_inclusive": true
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "kf_d3m_primitives.data_preprocessing.data_typing.simon.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "volumes": {
                "type": "typing.Union[NoneType, typing.Dict[str, str]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "kf_d3m_primitives.data_preprocessing.data_typing.simon.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "volumes"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Learns column annotations using training data. Saves to apply to testing data.\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nReturns:\n    CallResult[None]\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "kf_d3m_primitives.data_preprocessing.data_typing.simon.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Add SIMON annotations\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- Input pd frame with metadata augmented\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "produce_metafeatures": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's best guess for the structural type of each input column.\n\nArguments:\n    inputs {Inputs} -- full D3M dataframe, containing attributes, key, and target\n\nKeyword Arguments:\n    timeout {float} -- timeout, not considered (default: {None})\n    iterations {int} -- iterations, not considered (default: {None})\n\nRaises:\n    PrimitiveNotFittedError: if primitive not fit\n\nReturns:\n    CallResult[Outputs] -- dataframe with two columns: \"semantic type classifications\" and \"probabilities\"\n        Each row represents a column in the original dataframe. The column \"semantic type\n        classifications\" contains a list of all semantic type labels and the column\n        \"probabilities\" contains a list of the model's confidence in assigning each\n        respective semantic type label"
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets primitive's training data\n\nArguments:\n    inputs {Inputs} -- D3M dataframe\n\nParameters\n----------\ninputs:\n    The inputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "add_semantic_types": "typing.Union[NoneType, typing.List[typing.List[str]]]",
            "remove_semantic_types": "typing.Union[NoneType, typing.List[typing.List[str]]]"
        }
    },
    "structural_type": "kf_d3m_primitives.data_preprocessing.data_typing.simon.SimonPrimitive",
    "description": "Simon uses a LSTM-FCN neural network trained on 18 different semantic types to infer the semantic\ntype of each column. A hyperparameter `return_result` controls whether Simon's inferences replace existing metadata,\nappend new columns with inferred metadata, or return a new dataframe with only the inferred columns.\n\nSimon can append multiple annotations if the hyperparameter `multi_label_classification` is set to 'True'.\nIf `statistical_classification` is set to True, Simon will use rule-based heuristics to label categorical and ordinal columns.\nFinally, the `p_threshold` hyperparameter varies the prediction probability threshold for adding annotations.\n\nThe following annotations will only be considered if `statistical_classification` is set to False:\n    \"https://metadata.datadrivendiscovery.org/types/AmericanPhoneNumber\",\n    \"http://schema.org/addressCountry\", \"http://schema.org/Country\",\n    \"http://schema.org/longitude\", \"http://schema.org/latitude\",\n    \"http://schema.org/postalCode\", \"http://schema.org/City\",\n    \"http://schema.org/State\", \"http://schema.org/address\", \"http://schema.org/email\",\n    \"https://metadata.datadrivendiscovery.org/types/FileName\"\n\nThe following annotations will only be considered if `statistical_classification` is set to True:\n    \"https://metadata.datadrivendiscovery.org/types/OrdinalData\",\n\nArguments:\n    hyperparams {Hyperparams} -- D3M Hyperparameter object\n\nKeyword Arguments:\n    random_seed {int} -- random seed (default: {0})\n    volumes {Dict[str, str]} -- large file dictionary containing model weights (default: {None})\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "09df67cbb13ed7c0acdcbefabd9fa5ce51f613a7358bddf5e7d9e9dea5550824"
}
